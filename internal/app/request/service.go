package request

import (
	"context"
	"errors"
)

type IRequestService interface {
	AddRequest(ctx context.Context, taskId int64, text string, callbackUrl string) error
}

type RequestService struct {
	r IRepository
}

func (r *RequestService) AddRequest(ctx context.Context, taskId int64, text string, callbackUrl string) error {
	if taskId > 0 {
		return r.r.AddRequest(ctx, taskId, text, callbackUrl)
	}
	return errors.New("Task ID <= 0")
}

func BuildRequestService() IRequestService {
	return NewRequestService(BuildRepository())
}

func NewRequestService(r IRepository) *RequestService {
	return &RequestService{
		r: r,
	}
}