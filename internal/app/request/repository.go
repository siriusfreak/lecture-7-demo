package request

import (
	"context"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/siriusfreak/lecture-7-demo/internal/pkg/db"
)

type IRepository interface {
	AddRequest(ctx context.Context, taskId int64, text string, callbackUrl string) error
}

type Repository struct {}

func (r *Repository) AddRequest(ctx context.Context, taskId int64, text string, callbackUrl string) error {
	_, err := sq.Insert("tasks").Columns("task_id", "text", "callback_url",
	).Values(taskId, text, callbackUrl).RunWith(db.GetDB(ctx)).PlaceholderFormat(sq.Dollar).ExecContext(ctx)

	return err
}

func BuildRepository() IRepository {
	return NewRepository()
}

func NewRepository() *Repository {
	return &Repository{}
}