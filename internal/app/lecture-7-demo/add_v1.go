package lecture_7_demo

import (
	"context"

	desc "gitlab.com/siriusfreak/lecture-7-demo/pkg/lecture-7-demo"
	"google.golang.org/protobuf/types/known/emptypb"
)

func (a *Lecture6DemoAPI)AddV1(ctx context.Context,
	req *desc.AddRequestV1) (*emptypb.Empty, error) {
	err := a.srv.AddRequest(ctx, req.ID, req.Text, req.CallbackURL)
	return &emptypb.Empty{}, err
}
