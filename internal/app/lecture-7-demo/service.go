package lecture_7_demo

import (
	"gitlab.com/siriusfreak/lecture-7-demo/internal/app/request"
	desc "gitlab.com/siriusfreak/lecture-7-demo/pkg/lecture-7-demo"
)


type Lecture6DemoAPI struct {
	desc.UnimplementedLecture6DemoServer
	srv request.IRequestService
}

func BuildLecture6DemoAPI() desc.Lecture6DemoServer {
	return &Lecture6DemoAPI{
		srv: request.BuildRequestService(),
	}
}



func NewLecture6DemoAPI(srv request.IRequestService) desc.Lecture6DemoServer {
	return &Lecture6DemoAPI{
		srv: srv,
	}
}
