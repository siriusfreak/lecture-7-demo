package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	api "gitlab.com/siriusfreak/lecture-7-demo/internal/app/lecture-7-demo"
	"google.golang.org/grpc"

	db "gitlab.com/siriusfreak/lecture-7-demo/internal/pkg/db"
	desc "gitlab.com/siriusfreak/lecture-7-demo/pkg/lecture-7-demo"
)

const (
	grpcPort = ":82"
	grpcServerEndpoint = "localhost:82"
)


func run(dbConn *sql.DB) error {
	listen, err := net.Listen("tcp", grpcPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer(grpc.ChainUnaryInterceptor(db.NewInterceptorWithDB(dbConn)))
	desc.RegisterLecture6DemoServer(s, api.BuildLecture6DemoAPI())

	if err := s.Serve(listen); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

	return nil
}

func runJSON() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mux := runtime.NewServeMux()


	opts := []grpc.DialOption{grpc.WithInsecure()}

	err := desc.RegisterLecture6DemoHandlerFromEndpoint(ctx, mux, grpcServerEndpoint, opts)
	if err != nil {
		panic(err)
	}

	err = http.ListenAndServe(":8081", mux)
	if err != nil {
		panic(err)
	}
}

func main() {
	err := sql.ErrNoRows
	if errors.Is(err, sql.ErrNoRows) {
		fmt.Println("No rows")
	}
	dbConn := db.Connect("postgres://postgres:postgres@localhost:5432/postgres?sslmode=disable")

	go runJSON()

	if err := run(dbConn); err != nil {

		log.Fatal(err)
	}
}
