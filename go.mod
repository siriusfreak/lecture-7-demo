module gitlab.com/siriusfreak/lecture-7-demo

go 1.16

require (
	github.com/Masterminds/squirrel v1.5.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.5.0
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.13.0
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/siriusfreak/lecture-7-demo/pkg/lecture-7-demo v0.0.0-00010101000000-000000000000
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210816183151-1e6c022a8912 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)

replace gitlab.com/siriusfreak/lecture-7-demo/pkg/lecture-7-demo => ./pkg/lecture-7-demo
